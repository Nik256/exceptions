package com.epam.exceptions;

public class SomeCustomException extends Exception {
    public SomeCustomException(Throwable cause) {
        super(cause);
    }
}